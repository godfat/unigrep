# -*- encoding: utf-8 -*-
# stub: unigrep 1.0.0 ruby lib

Gem::Specification.new do |s|
  s.name = "unigrep"
  s.version = "1.0.0"

  s.required_rubygems_version = Gem::Requirement.new(">= 0") if s.respond_to? :required_rubygems_version=
  s.authors = ["Lin Jen-Shin (godfat)"]
  s.date = "2013-09-29"
  s.description = "Print Unicode information matching description. (grep Unicode data)\n\n* Inspired by App::Uni <https://metacpan.org/module/App::Uni>\n* Data downloaded from <ftp://ftp.unicode.org/Public/UNIDATA/UnicodeData.txt>"
  s.email = ["godfat (XD) godfat.org"]
  s.executables = ["unigrep"]
  s.files = [
  ".gitignore",
  ".gitmodules",
  "LICENSE",
  "README",
  "README.md",
  "Rakefile",
  "bin/unigrep",
  "lib/unigrep.rb",
  "lib/unigrep/version.rb",
  "task/.gitignore",
  "task/gemgem.rb",
  "unigrep.gemspec"]
  s.homepage = "https://github.com/godfat/unigrep"
  s.licenses = ["Apache License 2.0"]
  s.require_paths = ["lib"]
  s.rubygems_version = "2.1.5"
  s.summary = "Print Unicode information matching description. (grep Unicode data)"
end
